﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spin : MonoBehaviour {

	void Rotate(int direction)
    {
        gameObject.GetComponent<Transform>().Rotate(Vector3.up, direction * 30.0f);
    }

    public void RotateLeft()
    {
        Rotate(-1);
    }

    public void RotateRight()
    {
        Rotate(1);
    }
}
